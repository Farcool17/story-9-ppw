from django.urls import path
from .views import loginview, logoutview


appname = 'story9ppw'

urlpatterns = [
   path('', loginview, name = 'startPage'),
   path('logout/',logoutview, name='logout')
]
