from django import forms

class loginForm(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Enter Username",
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Enter Password",
        'required': True,
    }))