from django.test import TestCase, Client
from django.http import HttpRequest
from .views import loginview

# Create your tests here.

class Story9(TestCase):

    def test_page_login(self):
        response = Client().get('/log-in/')
        self.assertEqual(response.status_code,200)
    
    def test_page_logout(self):
        response = Client().get('/log-in/logout')
        self.assertEqual(response.status_code,301)
    
    def test_page_template_logout(self):
        response = Client().get('/log-in/logout')
        self.assertEqual(response.status_code,301)

    def test_page_template(self):
        response = Client().get('/log-in/')
        self.assertTemplateUsed(response,'story9ppw/log-in.html')
    
    def test_header(self):
        request = HttpRequest()
        response = loginview(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Please Log In!",html_response)
