from django.shortcuts import render, redirect
from .forms import loginForm
from django.contrib.auth import authenticate, login, logout

#Create your views here.

def loginview(request):
    form = loginForm()
    flag = False
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password = passw)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/log-in')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/log-in')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'story9ppw/log-in.html',response)


def logoutview(request):
    logout(request)
    form = loginForm()
    response = {'form':form}
    return redirect('/log-in')
